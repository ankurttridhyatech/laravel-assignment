<?php

namespace App\Http\Middleware;

use Closure;

class adminloginin
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {   
        if(! Session::has('Adminstatus'))
        {
            return redirect('/Adminlogin');
        }
        return $next($request);
    }
}
