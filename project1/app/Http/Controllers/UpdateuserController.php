<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use Validator;


class UpdateuserController extends Controller
{

 public function updateform($id,Request $data)  
    {


     $user = new User();
     $res = $user->find($id);

     return view('admin.updateUserForm',['res' => $res]);
    }

  
   public function update(Request $req)
    {
     
       $id=$req->userid;
     $data=new User();


     $d=$data::find($id);

     $t=time();
  $img = $t.'_'.$req->file('image')->getClientOriginalName();

       $d->name = $req->name;
       $d->gender = $req->radio;
       $d->profileImage = $img;
       $d->mobile_no = $req->mobile;
       $d->email = $req->email;
       $d->save();
       $req->file('image')->move('image/',$img);
      return redirect('/Dashboard');
    }

}