<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use App\Admin;
use Session;

class Admincontroller extends Controller
{   
    public function login(Request $data)
	   {
	       //$req = new User();
	       $email = $data->email;
	       $password = $data->password;

	       $count = Admin::where('email', $email)->where('password', $password)->first();


	       if($count !='')
	       {
	         $data->Session()->put('adminname',$email);
	         $data->Session()->put('Adminstatus',true);
	           return redirect('/Dashboard');
	       }
	       else
	       {
		       return redirect('/Admin');

		   }
		} 

	public function logout()
	{
		Session::flush();
		return redirect('/Admin');
	}
}
