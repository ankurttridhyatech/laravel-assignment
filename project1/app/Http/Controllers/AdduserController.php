<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use Validator;

class AdduserController extends Controller
{
    public function adduser(Request $data)
    {   

        $validator=Validator::make($data->all(),[
            'name' => 'required|max:30|min:3',
            'radio' => 'required',
            'mobile' => 'required|digits:10',
            'email' => 'required|email|unique:users,email',
            'password' => 'required|confirmed|min:6',
            'image' => 'required',
            'status' => 'required',
        ]);

       if($validator->fails())
         {
           return redirect('/add_user')->withErrors($validator)->withInput();
         }


    	$t=time();
     	$img = $t.'_'.$data->file('image')->getClientOriginalName();

    	$iuser = new User();
    	$iuser->name = $data->name;
    	$iuser->gender = $data->radio;
    	$iuser->mobile_no = $data->mobile;
    	$iuser->email = $data->email;
    	$iuser->password = \Hash::make($data->password);
    	$iuser->profileImage = $img;
    	$iuser->status = $data->status;


    	$iuser->save();
    	$data->file('image')->move('image/',$img);
    	return redirect('/Dashboard');
    }
}
