<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;

class DeleteuserController extends Controller
{
    public function status (Request $data)
    {
    	$id = $data->id;
    	$req= new User();
    	$model= $req::find($id);

    	if($model->status == "Active")
    	{
    		$model->status ="Inactive";

    		$model->save();

    		return redirect('/Dashboard');
    	}
    	else
    	{

    		$model->status = "Active";

    		$model->save();

    		return redirect('/Dashboard');
    	}
    }


    public function delete (request $data)
    {
    	$id = $data->id;
    	$req= new User();
        
       $req::find($id)->delete();

      return redirect('/Dashboard');
      
    }
}
