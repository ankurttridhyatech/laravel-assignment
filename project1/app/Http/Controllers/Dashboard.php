<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use App\User;

class Dashboard extends Controller
{
    public function index()
    {
    	$user=new User();

    	$record=$user->get()->toArray();

    	return view('admin.index',['users'=> $record]);
    }
    public function addUser()
    {
    	return view('admin.addUserForm');
    }
    public function admin()
    {
    	return view('admin.loginadmin');
    }
    public function updateUser()
    {
        return view('admin.updateUserForm');
    }
     public function aprofile()
    {
        return view('admin.adminProfile');
    }

    public function user()
    {
        return view('user.loginuser');
    }
    
    public function uprofile()
    {
        return view('user.userProfile');
    }

 
}
