<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/Dashboard','Dashboard@index');
Route::get('/add_user','Dashboard@addUser');

Route::get('/Admin','Dashboard@admin');
Route::get('/status/{id}','DeleteuserController@status');
Route::get('/delete/{id}','DeleteuserController@delete');
Route::get('/update/{id}','UpdateuserController@updateform');
Route::post('/admin_login','AdminController@login');
Route::get('/logOut','AdminController@logout');
Route::post('/add_user_data','AdduserController@adduser');
Route::post('/update_user','UpdateuserController@update');
Route::get('/aprofile','Dashboard@aprofile');


Route::get('/', 'HomeController@index')->name('auth');


Auth::routes();

Route::get('/user','Dashboard@user');
Route::get('/uprofile','Dashboard@uprofile');
