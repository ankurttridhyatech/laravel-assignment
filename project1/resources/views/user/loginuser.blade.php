@extends('user.layout.master')
@section('title','Userlogin')
@section('content')     
     <div class="formm">

    {!! Form::open(['url' => '/userlogin', 'class' => 'form-horizontal', 'files' => true]) !!}

    <fieldset>
        
        <legend>User Login</legend>
        

         <!-- Email -->
        <div class="form-group">
            {!! Form::label('email', 'Email:', ['class' => 'col-lg-2 control-label']) !!}
            <div class="col-lg-6">
                {!! Form::email('email', $value = null, ['class' => 'form-control', 'placeholder' => 'email']) !!}

                
            </div>
        </div>


        <!-- Password -->
        <div class="form-group">
            {!! Form::label('password', 'Password:', ['class' => 'col-lg-2 control-label']) !!}
            <div class="col-lg-6">
                {!! Form::password('password',['class' => 'form-control', 'placeholder' => 'Password', 'type' => 'password']) !!}

            </div>
        </div>

         {!! Form::submit('Submit', ['class' => 'btn btn-lg btn-info pull-center'] ) !!}

	</fieldset>

    {!! Form::close()  !!}

</div>
@endsection
