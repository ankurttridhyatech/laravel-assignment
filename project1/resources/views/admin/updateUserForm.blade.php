@extends('admin.layout.master')
@section('title','Updateuser')
@section('content')   

 <link rel="../../apple-touch-icon" sizes="76x76" href="img/apple-icon.png">
  <link rel="icon" type="../../image/png" href="img/favicon.png">
  <link href="../../css/bootstrap.min.css" rel="stylesheet" />
  <link href="../../css/now-ui-dashboard.css?v=1.5.0" rel="stylesheet" />
  <!-- CSS Just for demo purpose, don't include it in your project -->
  <link href="../../demo/demo.css" rel="stylesheet" />
     <div class="formm">
    <center><img src="{{asset('image/'.$res['profileImage'])}}" width="150" style="border-radius: 50%;" /><p class="h3 mt-3 text-primary">{{$res->name}}</p></center>

    {!! Form::open(['url' => '/update_user', 'class' => 'form-horizontal', 'files' => true]) !!}

    <fieldset>

        <legend>Form</legend>
    {!! Form::hidden('userid', $res['id'], ['class' => 'form-control', 'placeholder' => 'name']) !!}
        <div class="form-group">
            {!! Form::label('name', 'Name:', ['class' => 'col-lg-2 control-label']) !!}
            <div class="col-lg-6">
                {!! Form::text('name', $value = $res->name, ['class' => 'form-control', 'placeholder' => 'name']) !!}
            </div>
        </div>

       <div class="form-group">
            {!! Form::label('radios', 'Gender:', ['class' => 'col-lg-2 control-label']) !!}
            <div class="col-lg-6">
                <div>
                    {!! Form::label('male', 'male') !!}
                    {!! Form::radio('radio', 'male') !!}


                    {!! Form::label('female', 'female') !!}
                    {!! Form::radio('radio', 'female') !!}
                </div>
            </div>
        </div>

         

		<!-- Mobile-Number -->
        <div class="form-group">
            {!! Form::label('mobile', 'Mobile-no:', ['class' => 'col-lg-2 control-label']) !!}
            <div class="col-lg-6">
                {!! Form::number('mobile', $value = $res->mobile_no, ['class' => 'form-control', 'placeholder' => 'Mobile-no']) !!}
            </div>
        </div>
        <!-- Email -->
        <div class="form-group">
            {!! Form::label('email', 'Email:', ['class' => 'col-lg-2 control-label']) !!}
            <div class="col-lg-6">
                {!! Form::email('email', $value = $res->email, ['class' => 'form-control', 'placeholder' => 'email']) !!}
            </div>
        </div>


        <!-- Password -->
        

		 <!-- Profile image -->
      
            {!! Form::label('file', 'Profile_image:', ['class' => 'col-lg-2 control-label']) !!}
         
                {!! Form::file('image') !!}
         

        <!-- Submit Button -->
     
                {!! Form::submit('Submit', ['class' => 'btn btn-lg btn-info pull-center'] ) !!}
        
    </fieldset>

    {!! Form::close()  !!}

</div>
@endsection
