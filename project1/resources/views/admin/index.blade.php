@extends('admin.layout.master')
@section('content')  
@section('title','Dashboard')     

      <table class="table">
                    <thead class=" text-primary">
                      <th>Id</th>
                      <th>Name</th>
                      <th>Gender</th>
                      <th>Mobile_no</th>
                      <th>Email</th>
                      <th>Profile Image</th>
                      <th>Status</th>
                      <th>Update</th>
                      <th>Delete</th>
                    </thead>
                    <tbody>
                    	
                      @foreach($users as $data) 
           			 	<tr>

           			 			  </td>
		                		  <td>{{ $data['id'] }}</td>
		                		  <td>{{ $data['name'] }}</td>
		                          <td>{{ $data['gender'] }}</td>
		                          <td>{{ $data['mobile_no'] }}</td>
		                          <td>{{ $data['email'] }}</td>
           			 			  <td><img src="{{asset('image/'.$data['profileImage'])}}" width="50" /></td>

           			 			 <td> @if($data['status']=="Active")

                            <button class="btn btn-primary"><a class="text-white" href="/status/{{ $data['id'] }}">active</a></button>

                          @else

                             <button class="btn btn-danger"><a class="text-white" href="/status/{{ $data['id'] }}">Inactive</a></button>

                          @endif
                      </td>
							
		                      		<td><button type=""><a href="/update/{{ $data['id'] }}">Update</a></button></td>  
		            	 			<td><button type=""><a href="/delete/{{ $data['id'] }}">Delete</a></button></td>  
		            	 </tr>
		             @endforeach

		            </tbody>
           		
    			   
      </table>

@endsection