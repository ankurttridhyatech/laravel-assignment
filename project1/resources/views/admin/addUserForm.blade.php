@extends('admin.layout.master')
@section('title','Adduser')     
@section('content')     
     <b>Register</b>
  <div class="formm">

    {!! Form::open(['url' => '/add_user_data', 'class' => 'form-horizontal', 'files' => true]) !!}

    <fieldset>

        <legend>Form</legend>

        <div class="form-group">
            {!! Form::label('name', 'Name:', ['class' => 'col-lg-2 control-label']) !!}
            <div class="col-lg-6">
                {!! Form::text('name', $value = null, ['class' => 'form-control', 'placeholder' => 'name']) !!}
                @if(count($errors) > 0)
                @foreach($errors->get('name') as $er)
                    <p class="text-danger font-weight-bold mt-3">{{$er}}</p>
                @endforeach
            @endif
            </div>
        </div>

       <div class="form-group">
            {!! Form::label('radios', 'Gender:', ['class' => 'col-lg-2 control-label']) !!}
            <div class="col-lg-6">
                <div>
                    {!! Form::label('male', 'male') !!}
                    {!! Form::radio('radio', 'male') !!}


                    {!! Form::label('female', 'female') !!}
                    {!! Form::radio('radio', 'female') !!}

                    @if(count($errors) > 0)
                @foreach($errors->get('radio') as $er)
                    <p class="text-danger font-weight-bold mt-3">{{$er}}</p>
                @endforeach
            @endif
                </div>
            </div>
        </div>

         <div class="form-group">
            {!! Form::label('radios', 'Status:', ['class' => 'col-lg-2 control-label']) !!}
            <div class="col-lg-6">
                <div>
                    {!! Form::label('Active', 'Active') !!}
                    {!! Form::radio('status', 'Active') !!}


                    {!! Form::label('Inactive', 'Inactive') !!}
                    {!! Form::radio('status', 'Inactive') !!}

                    @if(count($errors) > 0)
                @foreach($errors->get('status') as $er)
                    <p class="text-danger font-weight-bold mt-3">{{$er}}</p>
                @endforeach
            @endif
                </div>
            </div>
        </div>

		<!-- Mobile-Number -->
        <div class="form-group">
            {!! Form::label('mobile', 'Mobile-no:', ['class' => 'col-lg-2 control-label']) !!}
            <div class="col-lg-6">
                {!! Form::number('mobile', $value = null, ['class' => 'form-control', 'placeholder' => 'Mobile-no']) !!}

                @if(count($errors) > 0)
                @foreach($errors->get('mobile   ') as $er)
                    <p class="text-danger font-weight-bold mt-3">{{$er}}</p>
                @endforeach
            @endif
            </div>
        </div>
        <!-- Email -->
        <div class="form-group">
            {!! Form::label('email', 'Email:', ['class' => 'col-lg-2 control-label']) !!}
            <div class="col-lg-6">
                {!! Form::email('email', $value = null, ['class' => 'form-control', 'placeholder' => 'email']) !!}

                @if(count($errors) > 0)
                @foreach($errors->get('email') as $er)
                    <p class="text-danger font-weight-bold mt-3">{{$er}}</p>
                @endforeach
            @endif
            </div>
        </div>


        <!-- Password -->
        <div class="form-group">
            {!! Form::label('password', 'Password:', ['class' => 'col-lg-2 control-label']) !!}
            <div class="col-lg-6">
                {!! Form::password('password',['class' => 'form-control', 'placeholder' => 'Password', 'type' => 'password']) !!}

                @if(count($errors) > 0)
                @foreach($errors->get('password') as $er)
                    <p class="text-danger font-weight-bold mt-3">{{$er}}</p>
                @endforeach
            @endif

            </div>
        </div>

        <div class="form-group">
            {!! Form::label('password', 'Confirm-Password:', ['class' => 'col-lg-2 control-label']) !!}
            <div class="col-lg-6">
                {!! Form::password('password_confirmation',['class' => 'form-control', 'placeholder' => 'Password', 'type' => 'password']) !!}

                @if(count($errors) > 0)
                @foreach($errors->get('password') as $er)
                    <p class="text-danger font-weight-bold mt-3">{{$er}}</p>
                @endforeach
            @endif

            </div>
        </div>

		 <!-- Profile image -->
      
            {!! Form::label('file', 'Profile_image:', ['class' => 'col-lg-2 control-label']) !!}
         
                {!! Form::file('image') !!}

                @if(count($errors) > 0)
                @foreach($errors->get('image') as $er)
                    <p class="text-danger font-weight-bold mt-3">{{$er}}</p>
                @endforeach
            @endif
         

        <!-- Submit Button -->
     
                {!! Form::submit('Submit', ['class' => 'btn btn-lg btn-info pull-center'] ) !!}
        
    </fieldset>

    {!! Form::close()  !!}

</div>
@endsection