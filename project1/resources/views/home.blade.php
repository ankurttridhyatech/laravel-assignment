@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">Dashboard</div>

                <div class="card-body">
                    @if (session('status'))
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }}
                        </div>
                    @endif  

      <table class="table">
                    <thead class=" text-primary">
                      
                      <tr>
                        <td><b>Profile Image</b></td>
                        <td><img src="img/img3.png" width="50"></td>
                      </tr><br><br>
                      
                      <tr>
                        <td><b>Name</b></td>
                        <td>{{ $user['name']}}</td>
                      </tr> <br><br>

                      <tr>
                        <td><b>Email</b></td>
                        <td>{{ $user['email']}}</td>
                      </tr><br><br>

        
                    </thead>
                    
                  
     </table>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
